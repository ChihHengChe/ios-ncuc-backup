//
//  ClassInfo.swift
//  TJCNCUCApp
//
//  Created by Andrew Che on 2016/7/16.
//  Copyright © 2016年 Andrew. All rights reserved.
//

import Foundation

class ClassInfo {
    var title:String
    var content:String
    var webLink:String
    
    
    init (title:String, content:String, webLink:String) {
        self.title = title
        self.content = content
        self.webLink = webLink
    }
}