//
//  NewsInfo.swift
//  TJCNCUCApp
//
//  Created by Andrew Che on 2016/7/16.
//  Copyright © 2016年 Andrew. All rights reserved.
//

import Foundation
import RealmSwift

class NewsInfo : Object {
    dynamic var id = 0
    dynamic var title:String = ""
    dynamic var imageLink:String = ""
    //var content:String
    dynamic var webLink:String = ""
    
    override class func primaryKey() -> String {
        return "id"
    }

}